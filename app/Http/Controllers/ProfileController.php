<?php

namespace Legabook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use Legabook\User;

class ProfileController extends Controller
{
    public function getProfile($username){

        $user = User::where('username',$username)->first();
        if (!$user)
        {
            abort(404);
        }

        $statuses = $user->statuses()->notReply()->orderBy('created_at','desc')->get();

        return view('profile.index')
            ->with('user',$user)
            ->with('statuses',$statuses)
            ->with('authUserIsFriend', Auth::user()->isFriendsWith($user));

    }

    public function getEdit(){
        return view('profile.edit');
    }

    public function postEdit(Request $request){

        $this->validate($request,[
            'firstname' => 'alpha|max:25',
            'lastname' => 'alpha|max:25',
            'location' => 'max:25',
        ]);

        Auth::user()->update([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'location' => $request->input('location'),
        ]);

        return redirect()->route('profile.index',['username' => Auth::user()->username]);
    }

    public function updateAvatar(Request $request){

        if ($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)
                ->orientate()
                ->resize(80,80)
                ->save( public_path('/uploads/avatars/' . $filename) );

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();

        }

        return redirect()->route('profile.index',['username' => Auth::user()->username]);



    }

}
