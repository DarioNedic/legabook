<?php

namespace Legabook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Legabook\User;

class SearchController extends Controller
{
    public function getResults(Request $request){

        $query = $request->input('query');

        if (!$query){
            return redirect()->back();
        }

        $users = User::where(DB::raw("CONCAT(firstname, '', lastname)"),'LIKE',"%{$query}%")
            ->get();

        return view('search.results')->with('users', $users);
    }
}
