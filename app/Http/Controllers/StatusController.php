<?php

namespace Legabook\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Legabook\Models\Like;
use Legabook\Models\Status;
use Legabook\User;

class StatusController extends Controller
{
    public function postStatus(Request $request) {

        $this->validate($request, [
           'status' => 'required|max:1000'
        ]);

        Auth::user()->statuses()->create([
            'body' => $request->input('status'),
        ]);

        return redirect()
            ->route('home')
            ->with('info','Status posted!');
    }

    public function postReply(Request $request, $statusId){

    $this->validate($request,[

        "reply-{$statusId}" => 'required|max:1000',
    ],[
        'required' => 'The reply body is required.'
    ]);
        $status = Status::notReply()->find($statusId);

        if(!$status) {
            return redirect() ->route('home');
        }
        //checking if not Auth user is friends with status owner
        if(!Auth::user()->isFriendsWith($status->user) && Auth::user()->id !==$status->user->id){
            return redirect()->back()->with('info',"Can't reply on non friends statuses!");
        }

        $reply = Status::create([
            'body'	=> $request->input("reply-{$statusId}"),
        ])->user()->associate(Auth::user());
        $status->replies()->save($reply);
        return redirect()->back();
    }

    public function getLike($statusId){

        $status = Status::find($statusId);

        if (!$status){
            return redirect()->route('home');
        }
        if (Auth::user()->hasLikedStatus($status)){
            return redirect()->back();
        }

        $like = $status->likes()->create([]);
        Auth::user()->likes()->save($like);

        return redirect()->back();

    }

    public function getUnlike($statusId){
        $status = Status::find($statusId);

        if (!$status){
            return redirect()->route('home');
        }

        if (!Auth::user()->hasLikedStatus($status)){
            return redirect()->back();
        }


        Auth::user()->likes()->where('likeable_id',$statusId)->delete();

        return redirect()->back();
    }

    public function deleteStatus($statusId){


        Auth::user()->statuses()->where('id',$statusId)->delete();

        return redirect()->back()->with('info','Post has been deleted');

    }

    public function showLikes($statusId){

        $status = Status::find($statusId);

        if (!$status){
            return redirect()->back();
        }

        $likers = Like::where('likeable_id',$status->id)->get();


        if($likers->isEmpty()){
            return redirect()->back()->with('info', 'Post has no likes!');
        }

        else {
            foreach ($likers as $liker) {
                    $users[] = User::where('id', $liker->user_id)->first();
            }
            return view('user.partials.likemodal')->with('users', $users);
        }
    }

}
