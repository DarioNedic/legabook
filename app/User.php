<?php

namespace Legabook;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Legabook\Http\Middleware\RedirectIfAuthenticated;
use Legabook\Models\Status;

class User extends Authenticatable
{
    use Notifiable;


    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'firstname', 'lastname', 'email', 'password', 'location', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getName()
    {
        if ($this->firstname && $this->lastname) {
            return "{$this->firstname} {$this->lastname}";
        }
        if ($this->firstname) {
            return $this->firstname;
        }
        return null;
    }

    public function statuses()
    {

        return $this->hasMany('Legabook\Models\Status','user_id');
    }

    public function likes(){

        return $this->hasMany('Legabook\Models\Like','user_id');
    }

    public function hasLikedStatus(Status $status)
    {
        return (bool) $status->likes->where('user_id', $this->id)->count();
    }


    public function friendsOfMine()
    {
        return $this->belongsToMany('Legabook\User', 'friends', 'user_id', 'friend_id');
    }

    public function friendOf()
    {
        return $this->belongsToMany('Legabook\User', 'friends', 'friend_id', 'user_id');
    }

    public function friends()
    {
        return $this->friendsOfMine()->wherePivot('accepted', true)->get()
            ->merge($this->friendOf()->wherePivot('accepted', true)->get());
    }

    public function friendRequests()
    {
        return $this->friendsOfMine()->wherePivot('accepted',false)->get();
    }

    public function friendRequestsPending()
    {
        return $this->friendOf()->wherePivot('accepted',false)->get();
    }

    public function hasFriendRequestPending(User $user)
    {
        return (bool) $this->friendRequestsPending()->where('id',$user->id)->count();
    }

    public function hasFriendRequestReceived(User $user)
    {
        return  (bool) $this->friendRequests()->where('id',$user->id)->count();
    }

    public function hasAnyFriendRequestsReceived(){ //checking if user has received any new friend request from anyone
        return (bool) $result = DB::table('friends')
            ->where('user_id', '=', Auth::user()->id)
            ->where('accepted', '=', 0)
            ->count();
    }

    public function addFriend(User $user)
    {
        $this->friendOf()->attach($user->id);
    }

    public function deleteFriend(User $user)
    {
        $this->friendOf()->detach($user->id);
        $this->friendsOfMine()->detach($user->id);
    }

    public function acceptFriendRequest(User $user)
    {
        $this->friendRequests()->where('id',$user->id)->first()->pivot->update([
            'accepted' => true,
        ]);
    }

    public function declineFriendRequest(User $user)
    {
        $this->friendRequests()->where('id',$user->id)->first()->pivot->delete();
    }


    public function isFriendsWith(User $user)
    {
        return (bool) $this->friends()->where('id',$user->id)->count();
    }


}