<?php

namespace Legabook\Models;

use Illuminate\Database\Eloquent\Model;
use Legabook\User;

class Like extends Model
{
    protected $table = 'likes';

    public function likeable(){
        return $this->morphTo(); //Polymorphic relationship,can be applied to any model
    }

    public function user(){
        return $this->belongsTo('Legabook\User','user_id');
    }
}
