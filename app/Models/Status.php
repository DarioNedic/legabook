<?php

namespace Legabook\Models;

use Illuminate\Database\Eloquent\Model;
use Legabook\User;

class Status extends Model
{
    protected $table = 'statuses';
    protected $fillable = [
        'body'
    ];
    public function user(){
        return $this->belongsTo('Legabook\User', 'user_id');
    }
    public function scopeNotReply($query) { //check if it's normal status and not a reply
        return $query->whereNull('parent_id');
    }

    public function replies(){
        return $this->hasMany('Legabook\Models\Status', 'parent_id');
    }

    public function likes(){
        return $this->morphMany('Legabook\Models\Like', 'likeable');
    }



    public function getBody()
    {
        // The Regular Expression filter
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

        // The Text you want to filter for urls
        $text = $this->body;

        // Check if there is a url in the text
        if (preg_match($reg_exUrl, $text, $url)) {

            // make the urls hyper links
            echo preg_replace($reg_exUrl, '<a target="_blank" href="' . $url[0] . '" rel="nofollow">' . $url[0] . '</a>', $text);

        } else {

            // if no urls in the text just return the text
            echo $text;
        }
    }
}
