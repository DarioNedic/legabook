@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-lg-6">
        <form role="form" method="post" action="{{ route('status.post') }}">
            <div class="form-group {{$errors->has('status') ? 'has-error':''}}">
                {!! csrf_field() !!}
                <textarea class="form-control" placeholder="What's up {{Auth::user()->getName() }} ?" name="status" rows='2'></textarea>
                @if ($errors->has('status'))
                    <span class="help-block">
						{{ $errors->first('status') }}
					</span>
                @endif
            </div>
            <button class="btn btn-default" type="submit"> Update Status</button>
        </form>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-lg-5">
        @if(!$statuses->count())
            <p>Nothing in your timeline.</p>
        @else
            @foreach($statuses as $status)
                @include('user.partials.status')
            @endforeach
                {!! $statuses->render() !!}
        @endif
    </div>
</div>
@stop