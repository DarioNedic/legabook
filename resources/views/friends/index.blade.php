@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <h3>Your friends</h3>

            @if (!$friends->count())
                <p>You have no friends.</p>
            @else
                @foreach($friends as $user)
                    @include ('user.partials.userblock')
                @endforeach
            @endif

        </div>
        <div class="col-lg-6">
            <h4>Friend requests</h4>
            @if (!$requests->count())
                <p>You have no friend requests.</p>
            @else
                @foreach($requests as $user )
                    @if (Auth::user()->hasFriendRequestReceived($user))
                        <div id="friendrequestblock">
                            @include('user.partials.userblock')
                                <div id="acceptdeclinebtn">
                                    <a href="{{ route('friends.accept',['username'=>$user->username]) }}" class="btn btn-primary">Accept</a>
                                    <a href="{{ route('friends.decline',['username'=>$user->username]) }}" class="btn btn-primary">Decline</a>
                                </div>
                        </div>
                        <hr>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
@stop