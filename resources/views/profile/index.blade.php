@extends('layouts.app')

@section('content')
    <h3>{{ $user->getName() }}'s profile:</h3>
    <br>
    <div class="row">
        <div class="col-lg-5">
            @include('user.partials.userblock')
            <hr>
            <h3>Statuses: {{$statuses->count()}}</h3>
            @if(!$statuses->count())

                <p>{{ $user->getName() }} hasn't posted anything yet.</p>
            @else
                @foreach($statuses as $status)
                    @include('user.partials.status')
                @endforeach
            @endif


        </div>
        <div class="col-lg-4 col-lg-offset-3">
            @if(Auth::user()->hasFriendRequestPending($user))
                <p>Waiting for {{ $user->getName() }} to accept your friend request.</p>
            @elseif (Auth::user()->hasFriendRequestReceived($user))
                <a href="{{ route('friends.accept',['username'=>$user->username]) }}" class="btn btn-primary">Accept friend request</a>
                <a href="{{ route('friends.decline',['username'=>$user->username]) }}" class="btn btn-primary">Decline friend request</a>
            @elseif (Auth::user()->isFriendsWith($user))
                <p>You and {{ $user->getName() }} are friends.</p>

                <form action="{{ route('friends.delete',['username'=>$user->username]) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="submit" class="btn btn-primary" value="Delete friend">
                </form>


            @elseif (Auth::user()->id !== $user->id)
                <a href="{{ route('friends.add',['username'=>$user->username]) }}" class="btn btn-primary">Add as friend</a>
            @endif

            <h4>{{ $user->getName() }} 's friends</h4>
            @if (!$user->friends()->count())
                <p>{{ $user->getName() }} has no friends.</p>
            @else
                @foreach($user->friends() as $user)
                    @include ('user.partials.userblock')
                @endforeach
            @endif
        </div>
    </div>
@stop