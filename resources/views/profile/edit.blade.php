@extends('layouts.app')
@section('content')
    <h3>Update your profile:</h3>
    <div class="row">
        <div class="col-lg-6">
            <form class="form-vertical" role="form" method="post" action="{{ route('profile.edit') }}">
                {{ csrf_field() }}
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label for="firstname" class="control-label">First name</label>
                        <input type="text" name="firstname" class="form-control" id="firstname" value="{{ Request::old('firstname') ?: Auth::user()->firstname }}">
                        @if ($errors->has('firstname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                        <label for="lastname" class="control-label">Last name</label>
                        <input type="text" name="lastname" class="form-control" id="lastname" value="{{ Request::old('lastname') ?: Auth::user()->lastname }}">
                        @if ($errors->has('lastname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form_group{{ $errors->has('location') ? ' has-error' : '' }}">
                <label for="location" class="control-label">Location</label>
                <input type="text" name="location" class="form-control" id="location" value="{{ Request::old('location') ?: Auth::user()->location }}">
                @if ($errors->has('location'))
                    <span class="help-block">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                @endif
            </div>
                <br>
            <div class="form-group">
                <button type="submit" class="btn btn-default">Update</button>
            </div>
            </form>
        </div>
        <div class="col-lg-6">
            <h2>User image</h2>
            <form action="{{ route("profile.avatar") }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Select image to upload as your new profile picture:</label>
                <input type="file" name="avatar" id="avatar">
                <br>
                <input type="submit" value="Upload" name="submit" class="btn btn-default" >
            </form>
        </div>
    </div>
@stop