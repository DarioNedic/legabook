<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/home') }}">
                {{ config('app.name', 'Legabook') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp; <!-- @if (Auth::check()) -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('home') }}">Timeline</a></li>
                    @if(Auth::user()->hasAnyFriendRequestsReceived())
                    <li><a href="{{ route('friends.index') }}">Friends  <i class="fa fa-bell fa-fw" aria-hidden="true"> </i></a></li>
                    @else
                    <li><a href="{{ route('friends.index') }}">Friends</a></li>
                        @endif
                </ul>

                <form action="{{route('search.results')}}" role="search" class="navbar-form navbar-left">
                    <div class="form-group">
                        <input type="text" name="query" id="searchbox" class="form-control"
                               placeholder="Find people"/>
                    </div>
                    <button type="submit" id="searchboxbtn" class="btn btn-default">Search</button>
                </form>
            <!-- @endif -->
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            @if(!Auth::user()->avatar)
                                <img class="media-object" alt="" id="dropdownimage" src="{{  Gravatar::src(Auth::user()->email)}}">
                            @else
                                <img class="media-object" alt="" id="dropdownimage" src="/uploads/avatars/{{Auth::user()->avatar}}">
                            @endif
                            {{ Auth::user()->firstname}} {{  Auth::user()->lastname  }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('profile.index',['username' =>Auth::user()->username]) }}">My profile</a></li>
                            <li><a href="{{route('profile.edit') }}">Edit profile</a></li>
                            <li>

                                <a href="{{ route('logout') }}"

                                   onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                    Logout
                                    <i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>