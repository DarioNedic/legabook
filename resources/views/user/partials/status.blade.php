<div class="media">
        <a class="pull-left" href="{{ route('profile.index',['username'=>$status->user->username]) }}">
            @if(!$status->user->avatar)
                <img class="media-object" alt="" src="{{  Gravatar::src($status->user->email)}}">
            @else
                <img class="media-object" alt="" src="/uploads/avatars/{{$status->user->avatar}}">
            @endif
        </a>
        <div class="media-body">
            @if($status->user->id === Auth::user()->id)
                <div class="deletedropdown">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span></a>

                        <ul class="dropdown-menu" role="menu" id="drop">
                            <li><a href="{{route('status.delete',['statusId' => $status->id]) }}">Delete post</a></li>
                        </ul>
                    </li>
                </div>
            @endif
            <h4 class="media-heading"><a href="{{ route('profile.index',['username'=>$status->user->username]) }}">{{ $status->user->getName() }}</a></h4>
            <p>{{ $status->getBody() }}</p>
            <ul class="list-inline">
                <li>{{ $status->created_at->diffForHumans() }}</li>
                @if($status->user->id !== Auth::user()->id && !Auth::user()->hasLikedStatus($status) )
                    <li><a href="{{ route('status.like',['statusId' => $status->id]) }}">Like</a></li>
                    @elseif (Auth::user()->hasLikedStatus($status) )
                    <li><a href="{{ route('status.unlike',['statusId' => $status->id]) }}">Unlike</a></li>
                @endif
                <li><a href="{{ route('status.showlikes',['statusId' => $status->id]) }}">{{$status->likes->count()}} {{str_plural('like',$status->likes->count())}}</a></li>
            </ul>

            @foreach ($status->replies as $reply)
               @include('user.partials.statusreplybox')
            @endforeach
            @include('user.partials.statusreplyform')
        </div>
</div>