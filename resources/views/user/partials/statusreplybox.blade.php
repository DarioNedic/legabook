<div class="media">
    <a href="{{ route('profile.index',['username'=> $reply->user->username]) }}" class="pull-left">
        @if(!$reply->user->avatar)
            <img src="{{  Gravatar::src($reply->user->email,50)}}" alt="{{ $reply->user->getName() }}" class="media-object">
        @else
            <img src="/uploads/avatars/{{$reply->user->avatar}}" alt="{{ $reply->user->getName() }}" class="media-object" style="height: 50px; width: 50px;">
        @endif
    </a>
    <div class="media-body">
        @if($reply->user->id === Auth::user()->id)
            <div class="deletedropdown">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span></a>

                    <ul class="dropdown-menu" role="menu" id="drop">
                        <li><a href="{{route('status.delete',['statusId' => $reply->id]) }}">Delete reply</a></li>
                    </ul>
                </li>
            </div>
        @endif
        <h4 class="media-heading"><a href="{{ route('profile.index',['username'=>$reply->user->username]) }}">
                {{$reply->user->getName() }}</a></h4>
        <p>{{$reply->getBody()}}</p>
        <ul class="list-inline">
            <li>{{ $reply->created_at->diffForHumans() }}</li>
            @if($reply->user->id !== Auth::user()->id && !Auth::user()->hasLikedStatus($reply) )
                <li><a href="{{ route('status.like',['statusId' => $reply->id]) }}">Like</a></li>
            @elseif (Auth::user()->hasLikedStatus($reply) )
                <li><a href="{{ route('status.unlike',['statusId' => $reply->id]) }}">Unlike</a></li>
            @endif
            <li><a href="{{ route('status.showlikes',['statusId' => $reply->id])}}"> {{$reply->likes->count()}} {{str_plural('like',$reply->likes->count())}}</a></li>
        </ul>
    </div>
</div>