<div class="media">
    <a class="pull-left" href="{{ route('profile.index',['username' => $user->username]) }}">
        @if(!$user->avatar)
            <img class="media-object" alt="" src="{{ Gravatar::src($user->email)}}">
        @else
            <img class="media-object" alt="" src="/uploads/avatars/{{$user->avatar}}">
        @endif
    </a>
    <div class="media-body">
        <h4 class="media-heading"><a href="{{ route('profile.index',['username' => $user->username]) }}">{{$user->getName()}}</a></h4>
        @if ($user->location)
            <p>{{ $user->location }}</p>
        @endif
    </div>
</div>