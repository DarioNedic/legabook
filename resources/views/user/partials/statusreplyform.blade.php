<form role="form" action="{{ route('status.reply',['statusId' => $status->id]) }}" method="post">
    {!! csrf_field() !!}
    <div class="form-group{{ $errors->has("reply-{$status->id}") ? ' has-error':"" }}">
        <textarea class="form-control" name="reply-{{$status->id}}" rows="2" placeholder="Reply to this status."></textarea>
        @if ($errors->has("reply-{$status->id}"))
            <span class="help-block">
            {{ $errors->first("reply-{$status->id}" ) }}
            </span>
        @endif
    </div>
    <input type="submit" class="btn btn-default btn-sm" value="Reply">
</form>