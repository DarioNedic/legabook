@extends('layouts.app')
@section('content')
<div>
    @foreach ($users as $user)
        @include('user.partials.userblock')
    @endforeach
</div>
@stop