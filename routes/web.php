<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Search route
Route::get('/search','SearchController@getResults')->name('search.results');

//User profile
Route::get('/user/{username}','ProfileController@getProfile')->name('profile.index');

//Following routes are protected by auth middleware
Route::group(['middleware' => 'auth'], function () {

    //User profile edit
    Route::get('/profile/edit', 'ProfileController@getEdit')->name('profile.edit');
    Route::post('/profile/edit', 'ProfileController@postEdit');
    Route::post('/profile/avatar', 'ProfileController@updateAvatar')->name('profile.avatar');

    //Friends page
    Route::get('/friends','FriendController@getIndex')->name('friends.index');
    Route::get('/friends/add/{username}','FriendController@getAdd')->name('friends.add');
    Route::get('/friends/accept/{username}','FriendController@getAccept')->name('friends.accept');
    Route::get('/friends/decline/{username}','FriendController@getDecline')->name('friends.decline');
    Route::post('/friends/delete/{username}','FriendController@postDelete')->name('friends.delete');


    //Statuses
    Route::post('/status','StatusController@postStatus')->name('status.post');
    Route::post('/status/{statusId}/reply','StatusController@postReply')->name('status.reply');
    Route::get('/status/{statusId}/delete','StatusController@deleteStatus')->name('status.delete');
    Route::get('/status/{statusId}/like','StatusController@getLike')->name('status.like');
    Route::get('/status/{statusId}/unlike','StatusController@getUnlike')->name('status.unlike');
    Route::get('/status/{statusId}/showlikes','StatusController@showLikes')->name('status.showlikes');

});

