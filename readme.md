# Legabook #

**Legabook** is social network application made in Laravel framework. It is based as Facebook but with lot less features.

**Users can:** 
- search other registered users on network
- add them as friends, decline or accept them
- see friends activity(statuses) on timeline
- visit friends profiles
- write statuses or reply to friends statuses, delete only their own statuses/replies
- like and unlike others statuses/replies
- edit their basic information under Edit Profile
- upload their own image as avatar under Edit Profile (avatars by default are set as Gravatar images)

**Install instructions:**
- Clone project with git clone
- Go to the project directory using cd
- Type composer install
- Copy .env.example as .env 
- Type php artisan key:generate
- Open your .env file and change the database name to whatever you have, username to root and password to root.
- Type php artisan migrate
- Go to localhost:8000